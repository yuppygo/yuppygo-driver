/**
 * Ionic 4 Taxi Booking Complete App (https://store.enappd.com/product/taxi-booking-complete-dashboard)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-documentmanagement',
  templateUrl: './documentmanagement.page.html',
  styleUrls: ['./documentmanagement.page.scss'],
})
export class DocumentmanagementPage implements OnInit {
  public documents = [{'name': 'Acuerdos legales', 'icon': 'person', 'url':'/drivinglicense'},
                      {'name': 'Licencia de conducir', 'icon': 'person', 'url': '/drivinglicense' },
                      {'name': 'Tarjeta de circulacion', 'icon': 'person', 'url': '/drivinglicense' },
                      {'name': 'Solvencia de antecedentes penales', 'icon': 'person', 'url': '/drivinglicense' },
                      {'name': 'Solvencia de antecedentes policiales', 'icon': 'person', 'url': '/drivinglicense' }]
  constructor(public route:Router) { }

  ngOnInit() {
  }
  gotoPage(item: any) {
      if (item.name === 'Acuerdos legales') {
      } else {
        this.route.navigate([item.url, { category:item.name } ]);

      }
  }
}
