/**
 * Ionic 4 Taxi Booking Complete App (https://store.enappd.com/product/taxi-booking-complete-dashboard)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */




import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';
import { UUID } from 'angular2-uuid';
import { UtilService } from '../util.service';
import { StorageService } from '../filestorage.service';

@Component({
  selector: 'app-drivinglicense',
  templateUrl: './drivinglicense.page.html',
  styleUrls: ['./drivinglicense.page.scss'],
})
export class DrivinglicensePage implements OnInit {
  public documents = [{
    'name': 'Actualizar',
    'icon': 'person',
  }]
  Card_number: any = '';
  Expiration_date: any = '';
  photos: any = [];
  category: any;
  existPhotos: boolean = false;
  private win: any = window;

  constructor(
    public actionCtrl: ActionSheetController, 
    public camera: Camera, 
    public Router: ActivatedRoute, 
    public auth: AuthService,
    public util:UtilService,
    private storageServ: StorageService,

    ) {
    this.Router.paramMap.subscribe(params => {
      this.category = params['params']['category'];
    });
  }

  ngOnInit() {
  }

  async openActionsheet() {
    this.openCamera();
    /*
    const action = await this.actionCtrl.create({
      buttons: [{
        text: 'Take a picture',
        role: 'destructive',
        cssClass: 'buttonCss',
        handler: () => {
          this.openCamera();
          console.log('Take a picture clicked');
        }
      }, {
        text: 'Choose a picture',
         handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'buttonCss_Cancel',
  
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await action.present();*/
  }
  async openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    await this.camera.getPicture(options).then((url) => {
      const file_id = UUID.UUID();
      const base64 = this.win.Ionic.WebView.convertFileSrc(url);
      this.util.makeFileIntoBlob(url, file_id).then(imageData => { this.managerArrayFiles('add', imageData, base64, file_id); })
    }, (err) => {
      console.log('' + err);
    });
  }

  async managerArrayFiles(action, imageData, base64, file_id) {

    let oldPhotos: any = this.photos;
    let newPhotos: any = [];
    console.log('action ' + action);
    if (action == 'add') {

      for (var i in oldPhotos) {
        let oldData = { file_id: oldPhotos[i].file_id, imageData: oldPhotos[i].imageData, base64: oldPhotos[i].base64 };
        newPhotos.push(oldData);
      }
      let newData = { file_id: file_id, imageData: imageData,base64: base64 };
      newPhotos.push(newData);
      this.photos = newPhotos;

    }
    if (action == 'delete') {

      newPhotos = oldPhotos.filter(function (el) { return el.file_id != file_id; });
      this.photos = newPhotos;

    }
    this.existPhotos = this.photos.length>0 ? true : false;
    console.log('this.photos->'+this.photos);
    console.log('this.photos->'+JSON.stringify(this.photos));
  }
  deleteItemFiles(file_id) {
    this.managerArrayFiles('delete', '0', '0', file_id);
  }
  updateData(){
    /*
      this.util.openInfLoader();
      this.storageServ.uploadDriverDocuments(this.photos).then(
        success => {
          this.util.closeLoading()
          this.util.presentToast('image uploded', true, 'bottom', 2100);
          console.log('success', success);
        }

      ).catch(err => {
        this.util.closeLoading();
        this.util.presentToast(`${err}`, true, 'bottom', 2100);
        console.log('err', err);
      })
    */
  }
}
