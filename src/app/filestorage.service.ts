/**
* Ionic Full App  (https://store.enappd.com/product/ionic-full-app-ionic-4-full-app)
*
* Copyright © 2019-present Enappd. All rights reserved.
*
* This source code is licensed as per the terms found in the
* LICENSE.md file in the root directory of this source tree.
*/
import { Injectable } from '@angular/core';
// import { AngularFireStorage } from 'angularfire2/storage';
import { AngularFireStorage } from '@angular/fire/storage';
import { FirestoreService } from './firestore.service'
import { AuthService } from './auth.service';
@Injectable()
export class StorageService {
    constructor(private storage: AngularFireStorage, private baseFs: FirestoreService, private auth: AuthService) {
    }
    public uploadContent(file: any, fileName: any): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                if (file) {
                    return this.storage.upload(`uploads/${fileName}`, file).then(
                        success => {
                            return this.storage.ref(`uploads/${fileName}`).getDownloadURL().subscribe(url => {
                                return this.baseFs.uploadFile('uploads', url, fileName)
                                    .then(() => {
                                        resolve({ url: url, fileName: fileName });
                                    })
                                    .catch(err => {

                                        reject(err);
                                    })
                            });
                        },
                        failure => {

                            reject(failure);
                        }
                    )
                        .catch(err => {

                            reject(err);
                        })
                } else {
                    reject(new Error(' choice key not given'));
                }
            } catch (e) {

                reject(e);
            }

        })
    }
    public uploadDriverDocuments(photos: any): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                if (photos) {
                    return this.storage.upload(`driverDocuments/${this.auth.loggedInUser.id}`, photos.imageData).then( success => {
                        return this.storage.ref(`driverDocuments/${this.auth.loggedInUser.id}`).getDownloadURL().subscribe(url => {
                            return this.baseFs.uploadDriverDocuments('driverDocuments', url, photos.file_id).then(() => {resolve({ url: url, file_id: photos.file_id }); })
                            .catch(err => { reject(err); }) 
                        });
                    },failure => { reject(failure); })
                    .catch(err => { reject(err); })
                } else { reject(new Error(' choice key not given')); }
            } catch (e) { reject(e); }
        })
    }
}
