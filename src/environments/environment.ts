/**
 * Ionic 4 Taxi Booking Complete App (https://store.enappd.com/product/taxi-booking-complete-dashboard)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
export const environment = {
  production: false,
  //add your firebase Project configuration
  // Take help from this Blog to setup firbase config:- https://enappd.com/blog/how-to-integrate-firebase-in-ionic-4-apps/23/
  config: {
    apiKey: "AIzaSyDuMptpAmMRDOr7bU7NReil1NGDAxYyP-g",
    authDomain: "yuppy-2cbaa.firebaseapp.com",
    databaseURL: "https://yuppy-2cbaa.firebaseio.com",
    projectId: "yuppy-2cbaa",
    storageBucket: "yuppy-2cbaa.appspot.com",
    messagingSenderId: "266934478758",
    appId: "1:266934478758:web:c38260cb8ea87ac3591167",
    measurementId: "G-7CPR085RC1"
  }
};
